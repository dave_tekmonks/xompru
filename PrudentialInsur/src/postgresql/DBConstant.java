package postgresql;

public class DBConstant {
	public static final String DB_DRIVER = "org.postgresql.Driver";
	public static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
	public static final String DB_USER = "postgres";
	public static final String DB_PASSWORD = "123";
	
	public static final String CONTRACT_NUMBER="contract_number";
	public static final String LIFE_NUMBER="life_number";
	public static final String COVERAGE_NUMBER ="coverage_number";
	public static final String  RIDER_NUMBER="rider_number";
	public static final String RISK_STATUS="risk_status";
	public static final String PREMIUM_STATUS ="premium_status";
	public static final String ISSURANCE_DATE="issurance_date";
	public static final String COMPONENT_CODE="component_code";
	public static final String PRODUCT_CODE = "product_code";
	public static final String PREMIUM_END_DATE="premium_end_date";
	public static final String INSTALLMENT_PREMIUM="installment_premium";
	public static final String RISK_END_DATE ="risk_end_date";
	public static final String BILLING_FREQUENCY="billing_frequency";
	public static final String BATCH_TRANSACTION_CODE="batch_transaction_code";
	public static final String LIFE_CLIENT_NUM = "life_client_num";
	public static final String AGENT_NUMBER="agent_number";
	public static final String T2_UNIT_NUMBER="t2_unit_number";
	public static final String T3_UNIT_NUMBER="t3_unit_number";
	public static final String CURRENCY="currency";
	public static final String CHANNEL="channel";
	public static final String CHECK_QUERY = "SELECT CONTRACT_NUMBER,LIFE_NUMBER,COVERAGE_NUMBER,RIDER_NUMBER,RISK_STATUS,PREMIUM_STATUS,ISSURANCE_DATE,COMPONENT_CODE,PRODUCT_CODE,PREMIUM_END_DATE,INSTALLMENT_PREMIUM,RISK_END_DATE,BILLING_FREQUENCY,BATCH_TRANSACTION_CODE,LIFE_CLIENT_NUM,AGENT_NUMBER,T2_UNIT_NUMBER,T3_UNIT_NUMBER,CURRENCY,CHANNEL FROM SMARTRACKER.TMP_INPUT";
	
}
