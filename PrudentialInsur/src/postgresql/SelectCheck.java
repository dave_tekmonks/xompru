package postgresql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SelectCheck {
	public static void main(String[] args) {

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "123";

        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement pst = con.prepareStatement("SELECT policyNumber,lifeNumber,coverageNumber,riderNumber,policyStatus,premiumStatus,insuranceDate,componentCode,productCode,premcessdate,instprem,coverageReceiveDate,billingfrequency,transactionactionCode,lifeAssuredClientNumber,fcCode,t2Unit,t3Unit,currency,channel,palrInput FROM PRUSMART.INSURANCE");
                ResultSet rs = pst.executeQuery()) {

            while (rs.next()) {
            	 System.out.println(rs.getString("policyNumber") + "\t"
                         + rs.getString("lifeNumber") + "\t"
                         + rs.getString("coverageNumber") + "\t"
                         + rs.getString("riderNumber") + "\t"
                         + rs.getString("policyStatus") + "\t"
                         + rs.getString("premiumStatus") + "\t"
                         + rs.getString("insuranceDate") + "\t"
                         + rs.getString("componentCode") + "\t"
                        + rs.getString("productCode") + "\t"
                         + rs.getString("premcessdate") + "\t"
                         + rs.getString("instprem") + "\t"
                         + rs.getString("coverageReceiveDate") + "\t"
                         + rs.getString("billingfrequency") + "\t"
                         + rs.getString("transactionactionCode") + "\t"
                         + rs.getString("lifeAssuredClientNumber") + "\t"
                         + rs.getString("fcCode") + "\t"
                         + rs.getString("t2Unit") + "\t"
                         + rs.getString("t3Unit") + "\t"
                         + rs.getString("currency") + "\t"
                         + rs.getString("channel") + "\t"
                         + rs.getString("palrInput") + "\t"
                         );
               
                
            }

        } catch (SQLException ex) {
System.out.println("Error Occurred");
        }
    }

}
