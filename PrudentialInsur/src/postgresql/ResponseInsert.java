package postgresql;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;

import insurance.Premium;
import insurance.Sample;

public class ResponseInsert {
	static PreparedStatement pstmt = null;
	static int count = 0;
	
	public static void responseInsertion(Sample response) {
		
		Connection con = DBUtility.openConnection();
		CreateTable.tableCreation();
		try {
			
			pstmt = con.prepareStatement(DBUtility.responseQuery());	// Getting the dynamic SQL Query for Insert the response data.
			
			Field[] listfield = DBUtility.fieldsInResponse();		// Get all the field dynamically.
			int i = 0;
			for (Field field : listfield) {							// Iterate one by one.
				if(!field.getName().equals("serialVersionUID")){
					
					String fieldName = field.getName();					// here, Variable will come like claimBilledAmountJETEligibility.
					char firstChar = Character.toUpperCase(fieldName.charAt(0));
					fieldName = fieldName.substring(1);
					fieldName = "get"+Character.toUpperCase(firstChar)+fieldName;  // So, finally that Variable i made as getClaimBilledAmountJETEligibility (getter method form).
					
					if(field.getType().getSimpleName().equals("String")){		// Now checking for the method return type.
						
						Method method = response.getClass().getMethod(fieldName);	// This is the logic for how i invoked a java method when given the method name as a String. (Line no. 28)
						String str = (String)method.invoke(response);
						pstmt.setString(++i, str);				// Finally increasing the index and setting the value which I'm getting by calling invoke() method.
						
					} else if(field.getType().getSimpleName().equals("int")){	// These all are the same for checking the method return type.
						
						Method method = response.getClass().getMethod(fieldName);
						Integer integ = (Integer)method.invoke(response);
						pstmt.setFloat(++i, integ.intValue());				//  ---------------------
						
					} else if(field.getType().getSimpleName().equals("double")){	
						
						Method method = response.getClass().getMethod(fieldName);
						Double dobl = (Double)method.invoke(response);
						pstmt.setDouble(++i, dobl.doubleValue());
						
					}else if(field.getType().getSimpleName().equals("Date")){	
						
						Method method = response.getClass().getMethod(fieldName);
						Date date = (Date)method.invoke(response);
						if(date != null)
							pstmt.setDate(++i, new java.sql.Date(date.getTime()));
						else
							pstmt.setNull(++i, java.sql.Types.DATE);						
					} 
					else if (field.getType().getSimpleName().equals("boolean") || field.getType().getSimpleName().equals("Boolean")) {

						fieldName = fieldName.substring(3);
						fieldName = "is" + fieldName;
						Method method = response.getClass().getMethod(fieldName);
						Boolean binaryValue = (Boolean) method.invoke(response);
						String binaryString = binaryValue.toString();
						pstmt.setString(++i, binaryString);

					}
					
				}
			}
			
			int row = pstmt.executeUpdate();
			
			// After setting all the values finally pushing into the DB.
			count = count + row;
			System.out.println(count+" record(s) inserted...");
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
