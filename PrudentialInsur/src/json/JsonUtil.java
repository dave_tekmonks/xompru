package json;
import java.io.File;

import org.codehaus.jackson.map.ObjectMapper;

import insurance.Insurance;
import insurance.Premium;
import insurance.RequestMapper;
import insurance.ResponseMapper;
import insurance.Sample;
public class JsonUtil {
	static ObjectMapper mapper = null;

	public static boolean convertIntoJSON(Object obj, String claimNumber) {
		final String requestPath = "C://Request//New2018//";
		final String responsePath = "C://Response//New042018//";
		mapper = new ObjectMapper();
		try {
			String fileName = obj.getClass().getSimpleName() + claimNumber;
			if (obj instanceof Insurance) {
				RequestMapper param = new RequestMapper();
				param.setInsurance((Insurance) obj);
				mapper.defaultPrettyPrintingWriter().writeValue(new File(requestPath + fileName + ".json"), param);
				System.out.println("Request : "+mapper.defaultPrettyPrintingWriter().writeValueAsString(param));
			} else if (obj instanceof Sample) {

				ResponseMapper param = new ResponseMapper();
				param.setSample((Sample) obj);
				mapper.defaultPrettyPrintingWriter().writeValue(new File(responsePath + fileName + ".json"), param);
				System.out.println("Response :"+mapper.defaultPrettyPrintingWriter().writeValueAsString(param));
			}
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			mapper = null;
			obj = null;

		}
	}
}
