package insurance;

import java.sql.Date;
import java.util.TimeZone;

/**
 * 
 * @author abtuser1
 *
 */

public class Insurance {

	private String contract_Number;
	private String life_Number;
	private String coverage_Number;
	private String rider_Number;
	private String risk_Status;
	private String premium_Status;
	private String issurance_Date;
	private String component_Code;
	private String product_Code;
	private String premium_end_Date;
	private double installment_Premium;
	private  String risk_End_Date;
	private  String billing_Frequency;
	private String batch_transaction_Code;
	private String life_Client_Number;
	private String agent_Number;
	private String t2_Unit_Number;
	private String t3_Unit_Number;
	private String currency;
	private String channel;
	
	
	
	/**
	 * 
	 */
	
    

   public Insurance() {
		// TODO Auto-generated constructor stub
	}


	public Insurance(String contract_Number, String life_Number,
			String coverage_Number, String rider_Number, String risk_Status,
			String premium_Status, String issurance_Date, String component_Code,
			String product_Code, String premium_end_Date,
			double installment_Premium, String risk_End_Date,
			String billing_Frequency, String batch_transaction_Code,
			String life_Client_Number, String agent_Number,
			String t2_Unit_Number, String t3_Unit_Number, String currency,
			String channel) {
		super();
		this.contract_Number = contract_Number;
		this.life_Number = life_Number;
		this.coverage_Number = coverage_Number;
		this.rider_Number = rider_Number;
		this.risk_Status = risk_Status;
		this.premium_Status = premium_Status;
		this.issurance_Date = issurance_Date;
		this.component_Code = component_Code;
		this.product_Code = product_Code;
		this.premium_end_Date = premium_end_Date;
		this.installment_Premium = installment_Premium;
		this.risk_End_Date = risk_End_Date;
		this.billing_Frequency = billing_Frequency;
		this.batch_transaction_Code = batch_transaction_Code;
		this.life_Client_Number = life_Client_Number;
		this.agent_Number = agent_Number;
		this.t2_Unit_Number = t2_Unit_Number;
		this.t3_Unit_Number = t3_Unit_Number;
		this.currency = currency;
		this.channel = channel;
	}


	public String getContract_Number() {
		return contract_Number;
	}
	public void setContract_Number(String contract_Number) {
		this.contract_Number = contract_Number;
	}
	public String getLife_Number() {
		return life_Number;
	}
	public void setLife_Number(String life_Number) {
		this.life_Number = life_Number;
	}
	public String getCoverage_Number() {
		return coverage_Number;
	}
	public void setCoverage_Number(String coverage_Number) {
		this.coverage_Number = coverage_Number;
	}
	public String getRider_Number() {
		return rider_Number;
	}
	public void setRider_Number(String rider_Number) {
		this.rider_Number = rider_Number;
	}
	public String getRisk_Status() {
		return risk_Status;
	}
	public void setRisk_Status(String risk_Status) {
		this.risk_Status = risk_Status;
	}
	public String getPremium_Status() {
		return premium_Status;
	}
	public void setPremium_Status(String premium_Status) {
		this.premium_Status = premium_Status;
	}
	
	


	public  String getPremium_end_Date() {
		return premium_end_Date;
	}


	public void setPremium_end_Date(String premium_end_Date) {
		this.premium_end_Date = premium_end_Date;
	}


	public  String getRisk_End_Date() {
		return risk_End_Date;
	}


	public void setRisk_End_Date(String risk_End_Date) {
		this.risk_End_Date = risk_End_Date;
	}


	public String getIssurance_Date() {
		return issurance_Date;
	}


	public void setIssurance_Date(String issurance_Date) {
		this.issurance_Date = issurance_Date;
	}


	public String getComponent_Code() {
		return component_Code;
	}
	public void setComponent_Code(String component_Code) {
		this.component_Code = component_Code;
	}
	public String getProduct_Code() {
		return product_Code;
	}
	public void setProduct_Code(String product_Code) {
		this.product_Code = product_Code;
	}
	
	public double getInstallment_Premium() {
		return installment_Premium;
	}
	public void setInstallment_Premium(double installment_Premium) {
		this.installment_Premium = installment_Premium;
	}
	
	public String getBilling_Frequency() {
		return billing_Frequency;
	}
	public void setBilling_Frequency(String billing_Frequency) {
	this.billing_Frequency = billing_Frequency;
	}
	public String getBatch_transaction_Code() {
		return batch_transaction_Code;
	}
	public void setBatch_transaction_Code(String batch_transaction_Code) {
		this.batch_transaction_Code = batch_transaction_Code;
	}
	public String getLife_Client_Number() {
		return life_Client_Number;
	}
	public void setLife_Client_Number(String life_Client_Number) {
		this.life_Client_Number = life_Client_Number;
	}
	public String getAgent_Number() {
		return agent_Number;
	}
	public void setAgent_Number(String agent_Number) {
		this.agent_Number = agent_Number;
	}
	public String getT2_Unit_Number() {
		return t2_Unit_Number;
	}
	public void setT2_Unit_Number(String t2_Unit_Number) {
		this.t2_Unit_Number = t2_Unit_Number;
	}
	public String getT3_Unit_Number() {
		return t3_Unit_Number;
	}
	public void setT3_Unit_Number(String t3_Unit_Number) {
		this.t3_Unit_Number = t3_Unit_Number;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}


	
}
