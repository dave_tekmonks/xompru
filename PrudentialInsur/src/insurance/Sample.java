package insurance;

public class Sample {
	private String contract_Number;
	private String component_Code;
	private String rider_Number;
	private String coverage_Number;
	private String life_Number;
	private String risk_Status;
	private String agent_Number;
	private String t2_Unit_Number;
	private String t3_Unit_Number;
	private String concate;
	private String concatnew;
	
	
	public String getConcatnew() {
		return concatnew;
	}
	public void setConcatnew(String concatnew) {
		this.concatnew = concatnew;
	}
	public String getConcate() {
		System.out.println(concate);
		return concate;
	}
	public void setConcate(String concate) {
		this.concate = concate;
	}
	public String getContract_Number() {
		return contract_Number;
	}
	public void setContract_Number(String contract_Number) {
		this.contract_Number = contract_Number;
	}
	public String getComponent_Code() {
		return component_Code;
	}
	public void setComponent_Code(String component_Code) {
		this.component_Code = component_Code;
	}
	public String getRider_Number() {
		return rider_Number;
	}
	public void setRider_Number(String rider_Number) {
		this.rider_Number = rider_Number;
	}
	public String getCoverage_Number() {
		return coverage_Number;
	}
	public void setCoverage_Number(String coverage_Number) {
		this.coverage_Number = coverage_Number;
	}
	public String getLife_Number() {
		return life_Number;
	}
	public void setLife_Number(String life_Number) {
		this.life_Number = life_Number;
	}
	public String getRisk_Status() {
		return risk_Status;
	}
	public void setRisk_Status(String risk_Status) {
		this.risk_Status = risk_Status;
	}
	public String getAgent_Number() {
		return agent_Number;
	}
	public void setAgent_Number(String agent_Number) {
		this.agent_Number = agent_Number;
	}
	public String getT2_Unit_Number() {
		return t2_Unit_Number;
	}
	public void setT2_Unit_Number(String t2_Unit_Number) {
		this.t2_Unit_Number = t2_Unit_Number;
	}
	public String getT3_Unit_Number() {
		return t3_Unit_Number;
	}
	public void setT3_Unit_Number(String t3_Unit_Number) {
		this.t3_Unit_Number = t3_Unit_Number;
	}

}
